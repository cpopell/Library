The goal is to build an end to end experience that will allow for the editing of gitlab flavored markdown in real time, saving this markdown to the autocthono.us repo, having this push to penguin, and having penguin recompile.

1. Have Penguin recognize any deltas between PyMarkdown and GitLab Markdown
2. Have Penguin automatically recompile when new files are pushed into the relevant GitLab repo.
3. Use https://github.com/ncornette/Python-Markdown-Editor to edit markdown live, by distributing a package with the aforementioned fixes to PyMarkdown
4. Build a live editor for GitLab instances.